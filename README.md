This is a frame for mounting BigTreeTech GTR control boards, holding
a 4010 axial fan with a duct for cooling stepper drivers.

The duct can be printed with few layers and minimal infill.  It
is printed upside down.

The frame may also be used as a drill template for mounting the
board onto a flat surface, such as a panel on a 3D printer.

The duct and the frame should be printed with the same layer height,
and thick layers are actually a benefit during assembly. I used 0.3mm
layers with a 0.4mm nozzle.  You can see the parameters I used in the
3mf file.

For assembly:
* Screw M5x8 screws (or slightly longer) into the M5 holes in the
  sides of the block on the base, to cut threads into it
* Remove the M5 screws from the block
* Orient the base with the fan-mounting block opposite the
  power supply inputs
* Use M3 bolts through the GTR board, through the M3 clearance
  holes in the base, and fasten with nuts on the other side
* Put the fan into the end of the duct blowing air into the duct
* Slide the duct onto the base taking care of wire mounting (the
  interference fit between layers should hold it together)
* Fasten the duct onto the base using the M5 screws

The design was done in the
[realthunder branch of FreeCAD](https://github.com/realthunder/FreeCAD_assembly3/releases).

